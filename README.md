Website
=======
http://xerces.apache.org/xerces-c/

License
=======
Apache License Version 2.0 (see the file source/LICENSE)

Version
=======
3.1.2

Source
======
xerces-c-3.1.2.tar.xz (sha256: a847529ab6125f36039f54fa61f3d5043791accf7da2f43917cd2b49deb768f1)